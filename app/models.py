from app import db

# ROLE_USER = 0
# ROLE_ADMIN = 1

class Call(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    call_sid = db.Column(db.String(64), unique = True)
    call_status = db.Column(db.String(120), index = True)
    to_number = db.Column(db.String(120), index = True)
    selection = db.Column(db.String(120), index = True)
    start_time = db.Column(db.DateTime)
    def __repr__(self):
        return '<Call %r>' % (self.call_sid)    