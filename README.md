# Twilio Voice Notification Demo

## Description

Voice notifications are a means of notifying individuals via a phone call. There are quite a few use cases for voice notifications, including event notification, IVR based surveys, information broadcasting. In this demo, the callee will hear a brief message and will be prompted to input digits. Inputs and responses are recorded on the Results page.

A live version of this demo is available here: [http://damp-tundra-8062.herokuapp.com/](http://damp-tundra-8062.herokuapp.com/)


##  Demo Installation instructions
### Configuring the application

There are only a few steps for setting up this application. Twilio needs a Twilio number to make outbound phone calls. In the `config.py` file, set the TWILIO_PHONE_NUMBER variable to equal a phone number that you own. You can use a number [that you already own](https://www.twilio.com/user/account/phone-numbers/incoming), or you can [purchase a new number](https://www.twilio.com/help/faq/phone-numbers/how-do-i-use-the-api-to-search-for-and-buy-twilio-phone-numbers). Next, set the AUTH_TOKEN and ACCOUNT_SID to your own SID and AuthToken.


### Deploy to cloud hosting 

##### Heroku

Create a new Heroku instance
	
	$ heroku create twilio-voice-notification-demo
	
Create a new Heroku database:

	$ heroku addons:add heroku-postgresql:dev
	
Push the solution to Heroku:

	$ git push heroku master

And run an upgrade script

	$ heroku run upgrade

Your solution should now be up and running

##### or deploy with ngrok

As with many Twilio solutions, you can run this project locally, and the expose it to the public internet using ngrok. See more about ngrok at [www.ngrok.com](www.ngrok.com). Run the solution using the locally stored Python application:

	$ flask/bin/python run.py

and in a separate window, run ngrok

	$ ngrok INSERT_PORT_NUMBER